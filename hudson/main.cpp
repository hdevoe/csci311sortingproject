#include "main.h"
#include <chrono>
#include <iostream>
#include <time.h>
using namespace std;


//Randomize, Generate, and Print the Array
int* randomize(int* array, int n) {
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{	
		array[i] = rand() % 1000 + 1;		
	}	
	return array;
}

int* generate(int n) {
	int* array = new int[n];	
	return randomize(array, n);
}

void printout(int* array, int n) {
	for(signed int i = 0; i < n; i++) {
		cout<< array[i]<< " ";
	}
	cout<<endl;
}

//Sorting Algorithm 2- Insert Sort
void insertsort(int* array, int n) {
	int i,j;
	int x;
	for(i = 1; i < n; i++) {
		x = array[i];
		j = i-1;
		while (j>=0 && array[j] > x) {
			array[j+1] = array[j];
			j--;
		}
		array[j+1] = x;
	}
}

//Sorting Algorithm 3-Binary Insertion Sort
int binarySearch(int* a, int index, int low, int high)
{
    if (high <= low)
        return (index > a[low])?  (low + 1): low;
 
    int mid = (low + high)/2;
 
    if(index == a[mid])
        return mid+1;
 
    if(index > a[mid])
        return binarySearch(a, index, mid+1, high);
    return binarySearch(a, index, low, mid-1);
}
 
// Function to sort an array a[] of size 'n'
void binaryInsertionSort(int* a, int n)
{
    int i, loc, j, selected;
 
    for (i = 1; i < n; ++i)
    {
        j = i - 1;
        selected = a[i];
 
        // find location where selected sould be inseretd
        loc = binarySearch(a, selected, 0, j);
 
        // Move all elements after location to create space
        while (j >= loc)
        {
            a[j+1] = a[j];
            j--;
        }
        a[j+1] = selected;
    }
}

//Sorting Algorithm 4- Selection Sort
void selectionsort(int* array, int n) {
	int i, j, smallest;
	for(i = 0; i < n; i++) {
		smallest = i;
		for (j = i + 1; j < n; j++) {
			if(array[j] < array[smallest]) smallest = j;
		}
		int placeholder = array[i];
		array[i] = array[smallest];
		array[smallest] = placeholder;
	}
}

//Ryan's ***************************************************************
//Sorting Algorithm 7- Merge Sort 4 (linked list)

//Node structure for linked list
struct node
{
	int data;
	node *next;
};

//Creation of a new node
node* NewNode(int d)
{
	struct node *newNode = new node;
	newNode->data = d;
	newNode->next = NULL;
	return newNode;
}

//Adding data to end of linked list 
node* AddToList(node *tail, int input)
{
	struct node *newNode;
	newNode = NewNode(input);

	if (tail == NULL)
	{
		tail = newNode;
	}
	else
	{
		tail->next = newNode;
		tail = tail->next;
	}

	return tail;
}

node* Merge(node* h1, node* h2)
{
	node *t1 = new node;
	node *t2 = new node;
	node *temp = new node;

	if (h1 == NULL)	return h2;

	if (h2 == NULL) return h1;

	t1 = h1;

	while (h2 != NULL)
	{
		t2 = h2;

		h2 = h2->next;
		t2->next = NULL;

		if (h1->data > t2->data)
		{
			t2->next = h1;
			h1 = t2;
			t1 = h1;
			continue;
		}

	flag:
		if (t1->next == NULL)
		{
			t1->next = t2;
			t1 = t1->next;
		}
		else if ((t1->next)->data <= t2->data)
		{
			t1 = t1->next;
			goto flag;
		}
		else
		{
			temp = t1->next;
			t1->next = t2;
			t2->next = temp;
		}
	}

	return h1;
}


//Implementing Merge Sort 
void MergeSort(node **head)
{
	node *first = new node;
	node *second = new node;
	node *temp = new node;
	first = *head;
	temp = *head;

	if (first == NULL || first->next == NULL)
	{
		return;
	}
	else
	{
		while (first->next != NULL)
		{
			first = first->next;
			if (first->next != NULL)
			{
				temp = temp->next;
				first = first->next;
			}
		}
		second = temp->next;
		temp->next = NULL;
		first = *head;
	}

	MergeSort(&first);
	MergeSort(&second);

	*head = Merge(first, second);
}

void runMerge4(int n)
{
	int i, num;
	struct node *head = new node;
	struct node *tail = new node;
	head = NULL;
	tail = NULL;

	srand(time(NULL));
	
	for (i = 0; i < n; i++)
	{
		num = rand()%1000 + 1;
		tail = AddToList(tail, num);
		if (head == NULL)
			head = tail;
	}

	MergeSort(&head);


	while (head != NULL)
	{
		head = head->next;
	}
	delete head;
	delete tail;
}

// End of Merge4 **************************************************************

//Sorting Algorithm 8- Quick Sort 1
int partition1(int* array, int low, int high) {
	int i,j;
	int pivotpoint = (low+high)/2; 
	int pivotitem = array[low];
	j = low;
	for (i = low+1; i <= high; i++) {
		if(array[i] < pivotitem) {
			j++;
			int placeholder = array[i];
			array[i] = array[j];
			array[j] = placeholder;
		}
	}
	pivotpoint = j;
	int placeholder = array[low];
	array[low] = array[pivotpoint];
	array[pivotpoint] = placeholder;
	return pivotpoint;
} 

void quicksort1(int* array, int low, int high) {
	if(high>low) {
		int pivotpoint = partition1(array,low,high);
		quicksort1(array,low, pivotpoint-1);
		quicksort1(array,pivotpoint+1,high);
	}
}


//Sorting Algorithm 9- Quick Sort 2
int partition2(int* array, int low, int high) {
	int pivotpoint = (low+high)/2;
	int i,j;
	i = 0;
	int pivotint = low;
	do i++;
	while (i < high && array[i]<=pivotint);
	do j--;
	while (array[j]>pivotint);
	while (i<j) {
		int placeholder = array[i];
		array[i] = array[j];
		array[j]=placeholder;
		do i++;
		while (array[i]<=pivotint);
		do j--;
		while (array[j]>pivotint);
	}
	pivotpoint = j;
	int placeholder = array[low];
	array[low] = array[pivotpoint];
	array[pivotpoint] = placeholder;
	return pivotpoint; 
}

void quicksort2(int* array, int low, int high) {
	if(high>low) {
		int pivotpoint = partition1(array,low,high);
		quicksort2(array,low, pivotpoint-1);
		quicksort2(array,pivotpoint+1,high);
	}
}

//Ryan's *********************************************************************
//Sorting Algorithm 10- HeapSort
void heapify(int arr[], int n, int i)
{
	int largest = i;  
	int l = 2 * i + 1;
	int r = 2 * i + 2;

	if (l < n && arr[l] > arr[largest])
		largest = l;

	if (r < n && arr[r] > arr[largest])
		largest = r;

	if (largest != i)
	{
		std::swap(arr[i], arr[largest]);

		heapify(arr, n, largest);
	}
}
int* heapsort(int* arr, int n)
{
	for (int i = n / 2 - 1; i >= 0; i--)
		heapify(arr, n, i);

	for (int i = n - 1; i >= 0; i--)
	{
		std::swap(arr[0], arr[i]);

		heapify(arr, i, 0);
	}return arr;
}

//*****************************************************************************

//Sorting Algorithm 11- Radix Sort
int findmax(int* masterList, int n) {
	int max = masterList[0];
	
	for(int i = 1; i < n; i++) {
		if(masterList[i] > max) {
			max = masterList[i];	
		}
	}
	return max;
}

void listsort(int* masterList, int n, int exp) {

	int out[n];
	int i, list[10] = {0};

	for(i = 0; i < n; i++) {
		list[ (masterList[i]/exp)%10]++;
	}

	for(i = 1; i < 10; i++) {
		list[i] += list[i-1];
	}
	
	for(i = n-1; i >=0; i--) {
		out[list[ (masterList[i]/exp)%10]-1] = masterList[i];
		list[(masterList[i]/exp)%10]--;
	}

	for(i = 0; i < n; i++) {
		masterList[i] = out[i];	
	}
}

void radixsort(int* masterList, int n) {
	int m = findmax(masterList, n);
	for(int exp = 1; m/exp>0; exp *=10) {
		listsort(masterList, n, exp); 

	}
}

int main()
{	
	int *array;
	for (int n = 10; n <=10;n*=10){
		array = generate(n);
		cout<<"Selection Sort (n="<<n<<")" <<endl;
		long long int duration = 0;
		for(int i = 1; i <=1;i++)
		{
			if (n == 10) {
				cout<<"Unsorted: ";
				printout(array,n);
			} 
			auto start = chrono::high_resolution_clock::now();
			selectionsort(array,n);
			auto end = chrono::high_resolution_clock::now();
			if (n == 10) {
				cout<<"Sorted: ";
				printout(array,n);
			} 
			duration += chrono::duration_cast<chrono::nanoseconds>(end - start).count(); 
			randomize(array,n);
		}
		
	/*auto start = chrono::high_resolution_clock::now();
		runMerge4(n);
		auto end = chrono::high_resolution_clock::now();
		duration = chrono::duration_cast<chrono::nanoseconds>(end - start).count();*/ 
		long long int average = (duration / 1000);
		cout<<endl<<"Total Duration: "<<duration<<" nanoseconds"<<endl;
		cout<<"Average: "<<average<<" nanoseconds"<<endl<<endl;
	}

	delete []array;
	return 0;
}

