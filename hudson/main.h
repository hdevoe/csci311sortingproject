#ifndef MAIN_H
#define MAIN_H
#include <iostream>
#include <cstdio>
#include <ctime>

class Main {
	public:
	int* generate(int n);
	int* randomize(int n[], int input);
	void printout(int n[], int input);
	void insertsort(int n[], int input);
	void selectionsort(int n[], int input);
	void partition1(int n[], int min, int max);
	void quicksort1(int n[], int min, int max);	
	void quicksort2(int n[], int min, int max);
	void partition2(int n[], int min, int max);
	int* heapsort(int array[], int n);
	void heapify(int array[], int n, int i);
	int findmax(int n[], int input);
	void listsort(int n[], int input, int exp);
	void radixsort(int n[], int input);	

	//Ryan's additions (probably wrong)
	struct node;
	node* NewNode(int d);
	node* AddToList(node *tail, int data);
	node* Merge(node* h1, node* h2);
	void MergeSort(node **head);

};

#endif
