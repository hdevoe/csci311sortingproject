﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingProject
{
    class ExchangeSort
    {
        public int[] sort(int[] S)
        {

            int temp;
            int n = S.Length -1;
            for (int i = 0; i <= n; i++)
            {
                for (int j = i + 1; j <= n; j++)
                {
                    if(S[j] < S[i])
                    {
                        temp = S[j];
                        S[j] = S[i];
                        S[i] = temp;
                    }
                    
                }
             }return S;
        }
    }
}
