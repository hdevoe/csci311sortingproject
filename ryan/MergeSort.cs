﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingProject
{
    class MergeSort
    {
        public int[] sort(int[] S)
        {
            if (S.Length == 1)
            {
                return null;
            }
            //Console.Write("\nS: ");
            //foreach (int k in S)
            //{
            //    Console.Write(k + " ");
            //}
            int n = S.Length;
            double temp = Math.Floor(n / 2.0);
            int middle = Convert.ToInt32(temp);
            int end = n - middle;

            //Console.WriteLine();

            int[] U = new int[middle];
            int[] V = new int[end];

            //Console.Write("U: ");
            for (int i = 0; i < middle; i++)
            {
                U[i] = S[i];
            //    Console.Write(U[i] + " ");
            }

            //Console.WriteLine();
            //Console.Write("V: ");
            for (int i = 0; i < V.Length; i++)
            {
                V[i] = S[i + middle];
            //    Console.Write(V[i] + " ");
            }

            sort(U);
            sort(V);
            merge(middle, end, U, V, S);

            return S;
        }

        private void merge(int h, int m, int[] U, int[] V, int[] S)
        {
            int i = 0;
            int j = 0;
            int k = 0;

            while(i< h && j< m)
            {
                if(U[i] <= V[j])
                {
                    S[k] = U[i];
                    i++;
                } else
                {
                    S[k] = V[j];
                    j++;
                }
                k++;
            }
            while(i<h)
            {
                S[k] = U[i];
                i++;
                k++;
            }
            while (j < m)
            {
                S[k] = V[j];
                j++;
                k++;
            }

            //if(i>h)
            //{
            //    for(int n = j; n<h+m; n++)
            //    {
            //        int temp = k;
            //        S[k] = V[n];
            //    }
            //}
            //else
            //{
            //    for (int n = i; n < h + m; n++)
            //    {
            //        int temp = k;
            //        S[k] = V[n];
            //    }
            //}
            
        }
    } 
}
