﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SortingProject
{
    class Program
    {
        private static ExchangeSort es = new ExchangeSort();
        //private static HeapSort hs = new HeapSort();
        //private static InsertionSort ins = new InsertionSort();
        //private static InsertionSortBinary insb = new InsertionSortBinary();
        private static MergeSort ms = new MergeSort();
        private static MergeSort2 ms2 = new MergeSort2();
        //private static MergeSort4 ms4 = new MergeSort4();
        //private static QuickSort1 qs1 = new QuickSort1();
        //private static QuickSort2 qs2 = new QuickSort2();
        //private static RadixSort rs = new RadixSort();
        //private static SelectionSort ss = new SelectionSort();
        private static Random ran = new Random();
        private static Stopwatch stopwatch = new Stopwatch();
        private static TimeSpan ts = new TimeSpan();
        private static int[] S;
        private static double average;
        

        static void Main(string[] args)
        {
            Console.WriteLine("\n");
            //runExchangeSort();
            //runMergeSort();
            runMergeSort2();
            Console.WriteLine("Done.");
            Console.ReadLine();
        }

        public static void runExchangeSort()
        {
            Console.WriteLine("                Exchange Sort");
            //load arrays with random values
            for (int k = 100; k <= 100000; k *= 10)
            {
                S = new int[k];
                for (int i = 0; i < S.Length; i++)
                {
                    S[i] = ran.Next(0, k);
                }

                //clocks the elapsed time that it takes to sort the k-sized array 1000 times. 
                stopwatch.Start();
                for (int i = 0; i < 1000; i++)
                {
                    es.sort(S);
                }
                stopwatch.Stop();
                printOutput();

            }
        }

        public static void runMergeSort()
        {
            Console.WriteLine("                Merge Sort");
            //load arrays with random values
            for (int k = 100; k <= 100000; k *= 10)
            {
                S = new int[k];
                for (int i = 0; i < S.Length; i++)
                {
                    S[i] = ran.Next(0, k);
                }
                //clocks the elapsed time that it takes to sort the k-sized array 1000 times. 
                stopwatch.Start();
                for (int i = 0; i < 1000; i++)
                {
                    ms.sort(S);
                }                
                stopwatch.Stop();
                printOutput();
            }
        }
        public static void runMergeSort2()
        {
            Console.WriteLine("                Merge Sort 2");
            //load arrays with random values
            for (int k = 100; k <= 100000; k *= 10)
            {
                S = new int[k];
                for (int i = 0; i < S.Length; i++)
                {
                    S[i] = ran.Next(0, k);
                }

                //clocks the elapsed time that it takes to sort the k-sized array 1000 times. 
                stopwatch.Start();
                for (int i = 0; i < 1000; i++)
                {
                    ms2.sort(S, 0, (S.Length - 1));
                }                
                stopwatch.Stop();
                printOutput();
            }
        }

        //tests the entire array for non-decreasing sort order
        public static bool isSorted(int[] S)
        {
            for (int i = 0; i < S.Length; i++)
            {
                if(i+1 != S.Length)
                {
                    return true;
                }
                if (S[i] > S[i + 1])
                {
                    return false;
                }                
            }
            return true;
        }

        public static void printOutput()
        {
            if (isSorted(S))
            {
                //average is the milliseconds clocked, from start to finish, divided by 1000
                average = stopwatch.Elapsed.TotalMilliseconds / 1000;
                //Console.WriteLine("        Element Count: " + S.Length + "\n        Elapsed time: " + ts);
                Console.WriteLine("      Average time in millis for " + S.Length + " items: " + average);
            }
        }
    }
}
