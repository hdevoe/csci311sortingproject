﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingProject
{
    class MergeSort2
    {
        public int[] sort(int[] S, int low, int high)
        {
            int mid;
            if(low<high)
            {
                mid = (low + high) / 2;
                sort(S, low, mid);
                sort(S, mid + 1, high);
                merge2(S, low, mid, high);
            }
            return S;
        }

        private void merge2(int[] S, int low, int mid, int high)
        {
            int i = low;
            int j = mid+1;
            int k = low;

            int[] U = new int[S.Length];

            while(i <= mid && j <= high)
            {
                if(S[i] <= S[j])
                {
                    U[k] = S[i];
                    i++;
                }
                else
                {
                    U[k] = S[j];
                    j++;
                } k++;
            }
            //while (i < mid)
            //    {
            //    U[k] = S[i];
            //    i++;
            //    k++;
            //} while(j < high)
            //{ 
            //    U[k] = S[j];
            //    j++;
            //    k++;
            //}
        }
    }
}
